#pragma once

#include "ofMain.h"
#include <wiringPi.h>

class MotorController: public ofThread
{
	public:
		MotorController();
		void setup(int, int, int);
		void threadedFunction();
		void setSpeed(float);
		void moveForward(bool);

	private:
		void rotate();
		void stop();

		int *m_motorPins;
		bool m_isMoving;
		float m_speed;
		bool m_forward;
};
