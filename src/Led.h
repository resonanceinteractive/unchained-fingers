#pragma once

#include "ofMain.h"
#include <wiringPi.h>

class Led
{
	public:
		Led();
		void setup(int);
		void update(bool);

	private:
		int m_ledPin;
};
