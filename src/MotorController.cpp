#include "MotorController.h"

MotorController::MotorController()
{
	m_isMoving = false;
	m_motorPins = new int[3];
	m_forward = true;
	m_speed = 0.0;
}

void MotorController::setup(int e, int a, int b)
{
	m_motorPins[0] = e;
	m_motorPins[1] = a;
	m_motorPins[2] = b;

	wiringPiSetup();

	for(int i=0; i<3; i++)
	{
		pinMode(m_motorPins[i], OUTPUT);
		digitalWrite(m_motorPins[i], LOW);
	}
	startThread(false, true);
}

void MotorController::threadedFunction()
{
	while(isThreadRunning())
	{
		if(m_isMoving)
		{
			rotate();
		}
		sleep(1);
	}
}

void MotorController::rotate(){
	if(m_forward)
	{
		digitalWrite(m_motorPins[1], HIGH);
		digitalWrite(m_motorPins[2], LOW);
	}else
	{
		digitalWrite(m_motorPins[1], LOW);
		digitalWrite(m_motorPins[2], HIGH);
	}
	int maxD = 200;
	int d = (m_speed*maxD)/1;
	digitalWrite(m_motorPins[0], HIGH);
	sleep(d);
	digitalWrite(m_motorPins[0], LOW);
	sleep(maxD - d);
}

void MotorController::setSpeed(float speed)
{
	if(speed > -0.1 && speed < 0.1)
	{
		speed = 0.0;
		stop();
	}else if(speed >= 0.1)
	{
		m_forward  = true;
		m_isMoving = true;
	}else if(speed <= -0.1)
	{
		speed *= -1.0;
		m_forward = false;
		m_isMoving = true;
	}
	m_speed = speed;
}

void MotorController::stop()
{
	for(int i=0; i<3; i++)
	{
		digitalWrite(m_motorPins[i], LOW);
	}
	m_isMoving  = false;
}
