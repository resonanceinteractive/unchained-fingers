#include "PlayerController.h"

PlayerController::PlayerController()
{
	m_maxHits = 40.0;

	my_buttonA = new Button();
	my_buttonB = new Button();

	my_startingSensor = new Button();
	my_endingSensor = new Button();

	m_buttonAValue = new bool[2];
	m_buttonBValue = new bool[2];
	for(int i=0; i<2; i++)
	{
		m_buttonAValue[i] = false;
		m_buttonBValue[i] = false;
	}
	m_lastButtonPressed = 1;
	m_timer = ofGetElapsedTimeMillis();
	m_speed = 0.0;
	m_hits = 0;
}

void PlayerController::setup(int pinA, int pinB, int startingPin, int endingPin)
{
	my_buttonA->setup(pinA);
	my_buttonB->setup(pinB);

	my_startingSensor->setup(startingPin);
	my_endingSensor->setup(endingPin);
}


void PlayerController::update()
{
	my_buttonA->update();
	my_buttonB->update();

	my_startingSensor->update();
	my_endingSensor->update();

	setSpeed();
}

void PlayerController::setSpeed()
{
	int hitAdd = 0;

	// input
	m_buttonAValue[1] = my_buttonA->isPressed();
	m_buttonBValue[1] = my_buttonB->isPressed();

	if(m_buttonAValue[1] != m_buttonAValue[0])
	{
		if(m_buttonAValue[1])
		{
			if(m_lastButtonPressed == 2)
			{
				hitAdd = 1;
			}
			m_lastButtonPressed = 1;
		}
		m_buttonAValue[0] = m_buttonAValue[1];
	}else if(m_buttonBValue[1] != m_buttonBValue[0])
	{
		if(m_buttonBValue[1])
		{
			if(m_lastButtonPressed == 1)
			{
				hitAdd = 1;
			}
			m_lastButtonPressed = 2;
		}
		m_buttonBValue[0] = m_buttonBValue[1];
	}

	m_hits += hitAdd;
	if(m_hits < 0)
	{
		m_hits = 0;
	}

	// speed
	if((ofGetElapsedTimeMillis() - m_timer) > 1500)
	{
//		ofLog() << "Hits = " << m_hits << endl;
		if(m_hits > m_maxHits)
		{
			m_hits = m_maxHits;
		}
		m_speed = (m_hits * 1.0) / m_maxHits;
		m_hits = 0;
		m_timer = ofGetElapsedTimeMillis();
	}
}

float PlayerController::getSpeed(){
	return m_speed;
}

bool PlayerController::isAtStartingPoint()
{
	return my_startingSensor->isPressed();
}

bool PlayerController::isAtEndingPoint()
{
	return my_endingSensor->isPressed();
}
