#pragma once

#include "ofMain.h"
#include <wiringPi.h>

class Button
{
	public:
		Button();
		void setup(int);
		void update();
		bool isPressed();

	private:
		bool readButtonInput(int);

		int m_buttonPin;
		bool m_pressed;
};
