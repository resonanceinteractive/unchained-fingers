#include "Button.h"

Button::Button()
{
}

void Button::setup(int pin)
{
	wiringPiSetup();
	m_buttonPin = pin;
	pinMode(m_buttonPin, INPUT);

	pullUpDnControl(m_buttonPin, PUD_UP);
}


void Button::update()
{
	m_pressed = readButtonInput(m_buttonPin);
}

bool Button::readButtonInput(int btn){
	bool buttonValue = true;

	if(digitalRead(btn) != 0)
	{
		buttonValue = false;
	}
	return buttonValue;
}

bool Button::isPressed(){
	return m_pressed;
}
