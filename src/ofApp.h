#pragma once

#include "ofMain.h"
#include "PlayerController.h"
#include "MotorController.h"
#include "Led.h"
#include "Button.h"

class ofApp : public ofBaseApp{
	public:
		void setup();
		void update();

	private:
		int playGame();
		void startCountdown();
		bool resetGame();

		int m_countdownStart;
		bool m_newGame;
		int m_gameMode;
		int m_gameModePrev;
		float p1SpeedPrev;
		PlayerController myPlayerController1;
		PlayerController myPlayerController2;

		MotorController myMotorController1;
		MotorController myMotorController2;

		Led myLed1;
		Led myLed2;
		bool m_led1UP;
		bool m_led2UP;
		Button startButton;
};
