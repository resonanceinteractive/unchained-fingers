 #include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	m_gameMode = 0; // 0 = reset // 1 = waiting start button // 2 = countdown // 3 = playing // 4 = race is over
	m_gameModePrev = -1;

	myMotorController1.setup(1, 0, 7);
	myMotorController1.setSpeed(0.0);

	myMotorController2.setup(4, 3, 2);
	myMotorController2.setSpeed(0.0);

	myPlayerController1.setup(26, 27, 12, 13);
	myPlayerController2.setup(28, 29, 14, 21);

	p1SpeedPrev = 0.0;

	myLed1.setup(23);
	myLed2.setup(24);

	m_led1UP = false;
	m_led2UP = false;

	m_countdownStart = 0;
	m_newGame = true;

	startButton.setup(22);
}

//--------------------------------------------------------------
void ofApp::update()
{
	myPlayerController1.update();
	myPlayerController2.update();

	startButton.update();

	myLed1.update(m_led1UP);
	myLed2.update(m_led2UP);

//--------------------------------------------------------------
	if(m_gameMode != m_gameModePrev)
	{
		m_gameModePrev = m_gameMode;
		ofLog() << "GameMode = " << m_gameMode << endl;
	}

	switch(m_gameMode)
	{
		case 0 :	// reset
		{
			bool reset = resetGame();
			if(reset)
			{
				m_gameMode++;
			}
			break;
		}
		case 1 :	// waiting new game
		{
			if(startButton.isPressed())
			{
				m_newGame = true;
				m_gameMode++;
			}
			break;
		}
		case 2 :	// countdown
		{
			if(m_newGame)
			{
				startCountdown();
				m_led1UP = false;
				m_led2UP = false;
				m_newGame = false;
			}

			int pCD = 2000; // preCountdownDelay
			int chrono = ofGetElapsedTimeMillis() - m_countdownStart;

			if(chrono >= pCD && chrono < (500+pCD))
			{
				m_led1UP = true;
				m_led2UP = true;

			}else if(chrono >= (500+pCD) && chrono < (1000+pCD))
			{
				m_led1UP = false;
				m_led2UP = false;
			}else if(chrono >= (1000+pCD) && chrono < (1500+pCD))
			{
				m_led1UP = true;
				m_led2UP = true;
			}else if(chrono >= (1500+pCD) && chrono < (2000+pCD))
			{
				m_led1UP = false;
				m_led2UP = false;
			}else if(chrono >= (2000+pCD) && chrono < (3000+pCD))
			{
				m_led1UP = true;
				m_led2UP = true;
			}else if(chrono >= (3000+pCD))
			{
				m_led1UP = false;
				m_led2UP = false;
				m_gameMode++;
			}
			break;
		}
		case 3 :	// playing
		{
			int winner = playGame();
			if(winner != 0)
			{
				if(winner == 1)
				{
					m_led1UP = true;
				}else if(winner == 2)
				{
					m_led2UP = true;
				}
				m_gameMode++;
			}

			if(startButton.isPressed())
			{
				m_gameMode++;
			}
			break;
		}
		case 4 : // race is over
		{
			myMotorController1.setSpeed(0.0);
			myMotorController2.setSpeed(0.0);

			bool newGame = startButton.isPressed();
			if(newGame)
			{
				m_gameMode = 0;
			}
			break;
		}
	}
}

int ofApp::playGame()
{
	int winner = 0;

	float p1Speed = myPlayerController1.getSpeed();
	float p2Speed = myPlayerController2.getSpeed();

	if(p1Speed != p1SpeedPrev /*&& p1Speed > 10.0*/)
	{
//		ofLog() << "Player 1 speed : " << p1Speed << endl;
//		p1SpeedPrev = p1Speed;
	}

	if(p1Speed > 1.0)
	{
		p1Speed = 1.0;
	}

	if(p2Speed > 1.0)
	{
		p2Speed = 1.0;
	}

	myMotorController1.setSpeed(p1Speed);
	myMotorController2.setSpeed(p2Speed);

	if(myPlayerController1.isAtEndingPoint())
	{
		winner = 1;
	}else if(myPlayerController2.isAtEndingPoint())
	{
		winner = 2;
	}

	return winner;
}

bool ofApp::resetGame()
{
	bool reset = false;

	bool p1Reset = myPlayerController1.isAtStartingPoint();
	bool p2Reset = myPlayerController2.isAtStartingPoint();

	if(!p1Reset)
	{
		myMotorController1.setSpeed(-1.0);
	}else
	{
		myMotorController1.setSpeed(0.0);
	}

	if(!p2Reset)
	{
		myMotorController2.setSpeed(-1.0);
	}else
	{
		myMotorController2.setSpeed(0.0);
	}

	if(p1Reset && p2Reset)
	{
		reset = true;
	}
	m_led1UP = false;
	m_led2UP = false;

	return reset;
}

void ofApp::startCountdown()
{
	ofResetElapsedTimeCounter();
	m_countdownStart = ofGetElapsedTimeMillis();
}
