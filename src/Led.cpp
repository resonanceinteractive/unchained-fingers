#include "Led.h"

Led::Led()
{
}

void Led::setup(int pin)
{
	wiringPiSetup();
	m_ledPin = pin;
	pinMode(m_ledPin, OUTPUT);
}


void Led::update(bool ledValue)
{
	digitalWrite(m_ledPin, ledValue);
}
