#pragma once

#include "ofMain.h"
#include "Button.h"

class PlayerController
{
	public:
		PlayerController();
		void setup(int, int, int, int);
		void update();
		float getSpeed();
		bool isAtStartingPoint();
		bool isAtEndingPoint();

	private:
		void setSpeed();

		Button* my_buttonA;
		Button* my_buttonB;

		Button* my_startingSensor;
		Button* my_endingSensor;

		bool *m_buttonAValue;
		bool *m_buttonBValue;
		int m_lastButtonPressed;
		int m_timer;
		float m_speed;
		int m_hits;
		float m_maxHits;
};
